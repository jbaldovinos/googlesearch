import unittest
import time
from selenium import webdriver
from pageObjects.HomePage import GoogleHomePage
from pageObjects.ResultPage import GoogleResultPage


class Test_GoogleSearch(unittest.TestCase):
    wordSearch = input("¿Que deseas buscar?  ")
    baseURL = "https://www.google.com/"
    driver = webdriver.Chrome('C:\\Users\\jbaldovinos\\Desktop\\drivers\\chromedriver.exe')

    @classmethod
    def setUpClass(cls) -> None:
        cls.driver.get(cls.baseURL)
        cls.driver.maximize_window()

    #Test the search with wordSearch value
    def test_search(self):
        googleHomePage = GoogleHomePage(self.driver)
        googleHomePage.sendInput(self.wordSearch)
        googleHomePage.clickSearch()
        self.assertEqual((self.wordSearch + " - Buscar con Google"), self.driver.title, "You are not in the search results")
        time.sleep(5)

    #Test if the correction link is present
    def test_correctionLink(self):
        googleHome = GoogleHomePage(self.driver)
        googleHome.correctioSearch()

    #Test the result list and print it
    def test_searchResult(self):
        googleResultList = GoogleResultPage(self.driver)
        pageResults = googleResultList.get_result_list(self.wordSearch)
        print(pageResults)

    @classmethod
    def tearDownClass(cls) -> None:
        cls.driver.quit()
        print("The browser is closed")