from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


class GoogleResultPage:

    def __init__(self, driver):
        self.driver = driver
        self.searchlist_Xpath = "//h3[@class='LC20lb DKV0Md']"
        self.resultList = None
        self.descriptionlist_Xpath = driver.find_elements_by_xpath("//span[@class='st']")
        self.descriptionResultList = None

    # This method prints the result list and validate that al results have their description
    def get_result_list(self, wordSearch):
        try:
            wait = WebDriverWait(self.driver, 40)
            self.resultList = wait.until(
                expected_conditions.presence_of_all_elements_located((By.XPATH, self.searchlist_Xpath)))
            data = []
            for item in self.resultList:
                data.append(item.text)
            print("Results available: ", len(data))

            if len(self.resultList) == len(self.descriptionlist_Xpath):
                print("All elements have description")
            else:
                print("Element not reachable")
            print(len(self.resultList), len(self.descriptionlist_Xpath))
            return data
        except:
            print("Incorrect List")
